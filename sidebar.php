<?php

/**
 * SIDEBAR
 *
 * A sidebar is any widgetized area of your theme. 
 * Widget areas are places in your theme where users can add their own widgets. 
 * 
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 */

 ?>

<?php if ( is_active_sidebar( 'shop' ) ) : ?>
  	<div id="sidebar-shop" class="sidebar" role="complementary">
  		<?php dynamic_sidebar( 'shop' ); ?>
  	</div><!-- #primary-sidebar -->
<?php else : ?>
    <!-- Time to add some widgets! -->
    <!-- <p>Place some widgets in wp-admin > Appaerence > Widgets.</p>     -->
<?php endif; ?>
