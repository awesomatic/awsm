<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awesomatic
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<header id="masthead" class="site-header">
			<div id="top-header" class="top-header">
				<div class="site-branding">
					<?php 
					/*
					 * 
					 */
					get_template_part( 'patterns/00-atoms/images/logo', 'light' ); ?>
				</div><!-- .site-branding -->

				<nav aria-label="Main Navigation" id="site-navigation" class="main-navigation">
					<?php 
					/*
					* framework > theming > menus.php
					*
					* Generated output: 
					* 
					* <ul>
					*	<li><a></a></li>
					* </ul>
					*/ 
					awsm_child_menu_header(); ?>
				</nav>

				<!-- <nav id="user-navigation" class="user-navigation"> -->
					<?php //awsm_child_menu_user(); ?>
				<!-- </nav> -->

				<nav id="shop-navigation" class="shop-navigation">
					<?php awsm_child_menu_shop(); ?>
				</nav>

				<?php // <div class="overlay">
				get_template_part( 'patterns/02-organisms/00-global/mobile-menu', 'overlay' ); ?>

				<?php // <button class="hamburger">
				get_template_part( 'patterns/00-atoms/buttons/button', 'hamburger' ); ?>

			</div> <!-- /top header -->
				
					<?php // wp-content/awesomatic-child/sidebar-header.php
					get_sidebar('header'); ?>
		</header>
