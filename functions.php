<?php
/**
 * Awesomatic functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Awesomatic
 * @since 0.5
 */

/**
 * TABLE OF CONTENTS
 *
 * CORE............................Everything thats needed to run a WordPress theme properly
 * Content Width...................Set the content width in pixels, based on the theme's design and stylesheet.
 * Setup...........................Sets up theme defaults and registers support for various WordPress features
 * Scripts.........................Enqueue scripts and styles
 *
 * HELPERS.........................General improvements to the WordPress system
 * Cleanup.........................Clean up WordPress Output
 * Dashboard.......................Remove unused dashboard widgets
 * Template Functions..............Functions which enhance the theme by hooking into WordPress
 *
 * CUSTOMIZER......................Tweak a theme's settings, color scheme or widgets, and see a preview of those changes in real time.
 * Customizer......................
 * Custom Header...................
 * Google Fonts....................
 *
 * THEMING.........................
 * Template Tags...................Custom template tags for this theme
 * 
 * BLOCKS..........................
 * Countdown.......................This block is useful when creating a coming soon page. 
 *
 * THEME OPTIONS...................Set up a WP-Admin page for managing turning on and off theme features.
 * Theme Options...................
 *
 * EXTENSIONS......................Everything related to WordPress plugins and vendors
 * Jetpack.........................Jetpack Compatibility File
 * Advanced Custom Fields..........Add a global options page to the ‘wp-admin’
 * WooCommerce.....................
 */

/**
 * CORE
 */

// Keep content width at the very top of your functions
require get_template_directory() . '/framework/core/content-width.php';
require get_template_directory() . '/framework/core/setup.php';
require get_template_directory() . '/framework/core/scripts.php';

/**
 * HELPERS
 */

require get_template_directory() . '/framework/helpers/cleanup.php';
require get_template_directory() . '/framework/helpers/dashboard.php';
require get_template_directory() . '/framework/helpers/template-functions.php';

/**
 * CUSTOMIZER
 */

require get_template_directory() . '/framework/customizer/customizer.php';
require get_template_directory() . '/framework/customizer/custom-header.php';
require get_template_directory() . '/framework/customizer/google-fonts.php';

/**
 * THEMING
 */

require get_template_directory() . '/framework/theming/template-tags.php';
require get_template_directory() . '/framework/theming/menus.php';

/**
 * BLOCKS
 */

/**
 * THEME OPTIONS
 */

//require get_template_directory() . '/framework/theme-options/theme-options.php';
require get_template_directory() . '/framework/theme-options/acf-options.php';
require get_template_directory() . '/framework/theme-options/acf-header.php';

/**
 * EXTENSIONS
 */

require get_template_directory() . '/framework/extensions/woocommerce.php';

/**
 * Public filters
 */

//add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

 