<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awesomatic
 */

?>

<footer id="colophon" class="site-footer">
  
  <?php // wp-content/awesomatic-child/sidebar-footer.php
  get_sidebar('footer'); ?>

  <div class="footer-colophon">
    <div class="copyright"><?php awsm_child_copyright(); ?> </div>
    <nav class="footer-navigation">
        <?php awsm_child_menu_footer(); ?>
    </nav>  
  </div>

  <!-- <div class="credits"> -->
    <?php //awsm_child_credits(); ?>
  <!-- </div> -->


</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
