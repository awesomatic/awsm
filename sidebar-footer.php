<?php

/**
 * SIDEBAR FOOTER
 *
 * A sidebar is any widgetized area of your theme. 
 * Widget areas are places in your theme where users can add their own widgets. 
 * 
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 */

 ?>

<?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
  	<div id="footer-sidebar" class="footer-sidebar widget-area" role="complementary">
  		<?php dynamic_sidebar( 'sidebar_footer' ); ?>
  	</div><!-- #primary-sidebar -->
<?php endif; ?>
