<?php

/**
 * REGISTER GUTENBERG ACF BLOCKS - COUNTDOWN
 * 
 * This block is useful when creating a coming soon page. 
 * Set the timer to a specific date to let it countdown until release.
 * 
 * @link https://www.advancedcustomfields.com/blog/acf-5-8-introducing-acf-blocks-for-gutenberg/
 */

function awsm_child_register_blocks() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(

        // (String) A unique name that identifies the block (without namespace). 
        // Lowercase alphanumeric characters and dashes, and must begin with a letter.
        'name'			    => 'countdown',
        
        // (String) The display title for your block. This is the title shown in the Gutenberg block editor.
        'title'			    => __('Countdown Timer'),

        // This is a short description for your block. This is the description shown in the Gutenberg editor.
        'description'		=> __('Een visuele klok die terugloopt tot een vastgestelde datum.'),

        // Blocks are grouped into categories to help users browse them. 
        // The core provided categories are [ common | formatting | layout | widgets | embed ]. 
        'category'		    => 'formatting',       
        
        // An icon property can be specified to make it easier to identify a block. https://developer.wordpress.org/resource/dashicons/
        'icon' => array(      

            // Specifying a background color to appear with the icon e.g.: in the inserter.
            'background' => '#7e70af',
            // Specifying a color for the icon (optional: if not set, a readable color will be automatically defined)
            'foreground' => '#fff',
            // Specifying a dashicon for the block
            'src' => 'clock',

        ),

        // An array of search terms to help user discover the block while searching.
        'keywords'		    => array( 'profile', 'user', 'author' ),
        
        // An array of post types to restrict this block type to.
        'post_types' => array('post', 'page'),

        // auto: Preview is shown by default but changes to edit form when block is selected. 
        // preview: Preview is always shown. Edit form appears in sidebar when block is selected. 
        // edit: Edit form is always shown
        'mode'			    => 'preview',

        'align' => 'full',

        // The path to a template file used to render the block HTML. 
        // This can either be a relative path to a file within the active theme or a full path to any file.
        'render_template'	=> 'patterns/02-organisms/blocks/block-timer.php'

        // Instead of providing a render_template, a callback function name may be specified to output the block’s HTML.
        //'render_callback' => 'my_acf_block_render_callback',
        
        // The url to a .css file to be enqueued whenever your block is displayed (front-end and back-end).
        //'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.css',
        
        // The url to a .js file to be enqueued whenever your block is displayed (front-end and back-end).
        //'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.js',
	));

}

add_action('acf/init', 'awsm_child_register_blocks' );
