<?php

/**
* Disable the message - JQMIGRATE: Migrate is installed, version 1.4.1
*/

add_action('wp_default_scripts', function ($scripts) {
  if (!empty($scripts->registered['jquery'])) {
    $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, ['jquery-migrate']);
  }
});
