<?php

/**
 *
 * Disable default dashboard widgets
 *
 */

  function awsm_disable_default_dashboard_widgets() {

  	remove_meta_box('dashboard_activity', 'dashboard', 'core');

  	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');

    // WordPress Events and news
  	remove_meta_box('dashboard_primary', 'dashboard', 'core');

    // Yoast SEO Widget - In some cases, you may need to replace 'side' with 'normal' or 'advanced'.
    remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );

  }

  add_action('admin_menu', 'awsm_disable_default_dashboard_widgets');

/**
 *
 * Custom dashboard widget
 *
 */

  function awsm_my_custom_dashboard_widgets() {

    global $wp_meta_boxes;

    wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'awsm_custom_dashboard_help');

  }

  function awsm_custom_dashboard_help() {

    echo '<p>Welcome to the Awesomatic Theme! Need help? Contact the developer <a href="mailto:jordi@awesomatic.nl">here</a>. For WordPress Tutorials visit: <a href="https://www.awesomatic.nl" target="_blank">Awesomatic</a></p>';

  }

  add_action('wp_dashboard_setup', 'awsm_my_custom_dashboard_widgets');

  /**
   * Hide WordPress update nag to all but admins
   */

  function hide_update_notice_to_all_but_admin() {
      if ( !current_user_can( 'update_core' ) ) {
          remove_action( 'admin_notices', 'update_nag', 3 );
      }
  }
  add_action( 'admin_head', 'hide_update_notice_to_all_but_admin', 1 );

  /**
 * Modify admin footer text
 */

function modify_footer() {
    echo 'Created by <a href="mailto:info@awesomatic.nl">Awesomatic</a>.';
}
add_filter( 'admin_footer_text', 'modify_footer' );

/**
 * Add thumbnail column to post listing
 */

add_image_size( 'admin-list-thumb', 80, 80, false );

function wpcs_add_thumbnail_columns( $columns ) {

    if ( !is_array( $columns ) )
        $columns = array();
    $new = array();

    foreach( $columns as $key => $title ) {
        if ( $key == 'title' ) // Put the Thumbnail column before the Title column
            $new['featured_thumb'] = __( 'Image');
        $new[$key] = $title;
    }
    return $new;
}

function wpcs_add_thumbnail_columns_data( $column, $post_id ) {
    switch ( $column ) {
    case 'featured_thumb':
        echo '<a href="' . $post_id . '">';
        echo the_post_thumbnail( 'admin-list-thumb' );
        echo '</a>';
        break;
    }
}

if ( function_exists( 'add_theme_support' ) ) {
    add_filter( 'manage_posts_columns' , 'wpcs_add_thumbnail_columns' );
    add_action( 'manage_posts_custom_column' , 'wpcs_add_thumbnail_columns_data', 10, 2 );
}

//
// Remove Admin Menu Items Depending on User Role
//

/**
 * Clone the administrator user role
 */

function clone_admin_role() {
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role( 'administrator' );

    // Add new "Client" role with all admin capabilities
    $wp_roles->add_role( 'client', 'Client', $adm->capabilities );
}
add_action( 'init', 'clone_admin_role' );

/**
 * Specify which admin menu items are visible for users with role "Client"
 */

function remove_dashboard_menus() {
    if ( current_user_can( 'client' ) ) {
        // Hide Updates under Dashboard menu
        //remove_submenu_page( 'index.php', 'update-core.php' );

        // Hide Comments
        //remove_menu_page( 'edit-comments.php' );

        // Hide Plugins
        //remove_menu_page( 'plugins.php' );

        // Hide Themes, Customizer and Widgets under Appearance menu
        //remove_submenu_page( 'themes.php', 'themes.php' );
        //remove_submenu_page( 'themes.php', 'customize.php?return=' . urlencode( $_SERVER['REQUEST_URI'] ) );
        //remove_submenu_page( 'themes.php', 'widgets.php' );

        // Hide Tools
        //remove_menu_page( 'tools.php' );

        // Hide General Settings
        //remove_menu_page( 'options-general.php' );
    }
}
add_action( 'admin_menu', 'remove_dashboard_menus' );
