<?php

/*
 * This function will add global options page to the ‘wp-admin’.
 * Requires at least ACF PRO version 5.0.0.
 * Must be used before the action admin_menu (priority 99) as this is when the options pages are added to WordPress.
 *
 * @url https://www.advancedcustomfields.com/resources/acf_add_options_page/
 */

if( function_exists('acf_add_options_page') ) {

	$option_page = acf_add_options_page(array(
		/* (string) The title displayed on the options page. Required. */
		'page_title' => 'Connect',

		/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
		'menu_title' => 'Connect',

		/* (string) The URL slug used to uniquely identify this options page.
		Defaults to a url friendly version of menu_title */
		'menu_slug' => 'connect_options',

		/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
		Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
		'capability' => 'edit_posts',

		/* (int|string) The position in the menu order this menu should appear.
		WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
		Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
		Defaults to bottom of utility menu items */
		'position' => false,

		/* (string) The slug of another WP admin page. if set, this will become a child page. */
		'parent_slug' => '',

		/* (string) The icon class for this menu. Defaults to default WordPress gear.
		Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
		'icon_url' => 'dashicons-megaphone',

		/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
		If set to false, this parent page will appear alongside any child pages. Defaults to true */
		'redirect' => true,

		/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
		Defaults to 'options'. Added in v5.2.7 */
		'post_id' => 'options',

		/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
		Defaults to false. Added in v5.2.8. */
		'autoload' => false,

		/* (string) The update button text. Added in v5.3.7. */
		'update_button'		=> __('Update', 'acf'),

		/* (string) The message shown above the form on submit. Added in v5.6.0. */
		'updated_message'	=> __("Options Updated", 'acf'),
	));

}

/*
 * Description: Add Google Analytics to your website using ACF Options.
 * Create an options field type text area field called analytics_code with no formatting output.
 */

	function awsm_add_google_analytics_code() {

		/*
		 * Create an ACF textfield named 'analytics_code' in:
		 * wp-admin > Extra fields > [your existing group] or create a new group
		 */

		// NEVER CHANGE - the 'analytics_code' name as it will erase all saved entries from your AWSM customers
		the_field('analytics_code', 'option');

	}

	add_action( 'wp_head', 'awsm_add_google_analytics_code', 10 );
