<?php

/**
 * Change the placeholder image
 */

function custom_woocommerce_placeholder_img_src( $src ) {
	$upload_dir = wp_upload_dir();
	$uploads = untrailingslashit( $upload_dir['baseurl'] );
	// replace with path to your image
	$src = $uploads . '/2012/07/thumb1.jpg';
	 
	return $src;
}

/*
 * Add cart menu item to main menu
 * 
 * Description: 
 * 
 * @Doc awesomatic.nl/docs/awsm
 */

function am_append_cart_icon( $items, $args ) {

	$cart_item_count = WC()->cart->get_cart_contents_count();
	$cart_count_span = '';
	if ( $cart_item_count ) {
		$cart_count_span = '<span class="count">'.$cart_item_count.'</span>';
	}
	$cart_link = '<li class="cart menu-item menu-item-type-post_type menu-item-object-page"><a href="' . get_permalink( wc_get_page_id( 'cart' ) ) . '"><i class="fa fa-shopping-bag"></i>'.$cart_count_span.'</a></li>';

	// Add the cart link to the end of the menu.
	$items = $items . $cart_link;

	return $items;

}
