<?php
/**
 * Enqueue scripts and styles.
 */
function awsm_scripts() {

	wp_enqueue_style('awsm-style', get_template_directory_uri() . '/style.css', array(), filemtime(get_template_directory() . '/style.css'), false);

	//wp_enqueue_style( 'awsm-fonts', awsm_fonts_url() );

	wp_enqueue_script( 'awsm-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'awsm-app', get_template_directory_uri() . '/assets/js/app.js', array(), '20190810', true );

	wp_enqueue_script( 'awsm-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'awsm_scripts' );
