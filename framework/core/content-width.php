<?php
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 * Content Width is a theme feature, first introduced in Version 2.6.
 * Using this feature you can set the maximum allowed width for any
 * content in the theme, like oEmbeds and images added to posts.
 *
 * @global int $content_width
 */
function awsm_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'awsm_content_width', 640 );
}

add_action( 'after_setup_theme', 'awsm_content_width', 0 );
