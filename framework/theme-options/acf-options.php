<?php
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Awesomatic Theme',
		'menu_title'	=> 'Starter Theme',
		'menu_slug' 	=> 'awsm-starter-theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> true,
		'icon_url' 		=> 'dashicons-admin-settings',
		'position' 		=> 205
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Documentatie',
		'menu_title'	=> 'Documentatie',
		'parent_slug'	=> 'awsm-starter-theme-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme header settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'awsm-starter-theme-options',
	));
}
