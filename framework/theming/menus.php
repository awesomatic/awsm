<?php

/**
 *
 * REGISTER MENUS
 *
 * Register our menus for use in wp-admin
 *
 */

    function awsm_child_register_nav_menus() {

        register_nav_menus(array(
            
            // The main menu
            'header'    => 'Header',    
            /**
             * The menu displaying when users click on the hamburger menu
             */
            'header-mobile'    => 'Mobile', 
            /**
             * The menu for displaying menu-items thats legally required,
             * for example: disclaimer, privacy, Terms & conditions, etc. 
             */ 
            'maintenance'    => 'Maintenance',

            //
            'user'    => 'User',

            'footer'    => 'Footer'

        ));

    }

add_action( 'init', 'awsm_child_register_nav_menus' );

/**
 * Build our header menu template tag
 *
 * Use <?php awsm_child_menu_header ?> to display your menu
 */

    function awsm_child_menu_header() {

        wp_nav_menu(array(

            'theme_location'    => 'header', // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
            
            'menu_class'        => 'menu-header',

            'container'         => '',

            // Don't show anything if no menu is assigned to this location

            'fallback_cb'       => false
            //'walker'          => new WPSE_78121_Sublevel_Walker
            
        ));

    }

    function awsm_child_menu_header_mobile() {

        wp_nav_menu(array(

            'theme_location'    => 'header-mobile', // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
            
            'menu_class'        => 'menu-header-mobile',

            'container'         => '',

            // Don't show anything if no menu is assigned to this location

            'fallback_cb'       => false
            //'walker'          => new WPSE_78121_Sublevel_Walker

        ));

    }

/**
* Custom walker
*/

class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu {

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='menu-primary-sub'><ul class='dropdown'>\n";
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

/**
 * Build our maintenance menu template tag
 *
 * Use <?php awsm_child_menu_maintenance ?> to display your menu
 */

    function awsm_child_menu_maintenance() {

        wp_nav_menu( array(

        // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
        'theme_location'    => 'maintenance',
        
        //
        'menu_class'      => 'menu',

        // Don't show anything if no menu is assigned to this location
        'fallback_cb'     => false
        ) );

    }

/**
 * Build our user menu template tag
 *
 * Use <?php awsm_child_menu_user ?> to display your menu
 */

    function awsm_child_menu_user() {

        wp_nav_menu( array(

        // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
        'theme_location'    => 'user',
        
        //
        'menu_class'      => 'menu',

        // Don't show anything if no menu is assigned to this location
        'fallback_cb'     => false
        ) );

    }

/**
 * Build our footer menu template tag
 *
 * Use <?php awsm_child_menu_footer ?> to display your menu
 */

    function awsm_child_menu_footer() {

        wp_nav_menu( array(

        // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
        'theme_location'  => 'footer',
        
        //
        'menu_class'      => 'menu',

        // Don't show anything if no menu is assigned to this location
        'fallback_cb'     => false
        ) );

    }



/**
 * Build our shop menu
 *
 * 
 */
 
    function awsm_child_menu_shop() { ?> 

        <ul class="shop-navigation">

        <?php if (get_field('awsm_show_mini_cart', 'option')) : ?>

            <li class="awsm-cart-summary">
                <ul id="awsm-mini-cart">
                    <li>
                        <a href="<?php echo wc_get_cart_url() ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/icon__shopping_cart.svg" width="24" alt="View cart" /></a>
                    </li>
                    <li>
                        <a class="cart-customlocation"></a>
                    </li>
                </ul>
            </li>

        <?php endif; ?>  
            
            <!-- <li>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/icon__shopping_favorites.svg" width="20" alt="Shopping favorites"> 
            </li> -->
            <li>
                <?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="user-logged--in" title="<?php _e('My Account','awsm-child'); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/icon__person.svg" width="18" alt="User account">
                    </a>
                <?php } 

                else { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="user-logged--out" title="<?php _e('Login / Register','awsm-child'); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/icon__person.svg" width="18" alt="User account"> 
                    </a>
                <?php } ?>
            </li>
            <li>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/icon__user_brightness.svg" width="20" alt="Brightness"> 
            </li>
        </ul>  
        
    <?php }
