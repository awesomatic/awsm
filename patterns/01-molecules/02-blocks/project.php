<?php

/**
 * Project Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'awsm-project-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'awsm-project';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php 

    //
    $args = array(
        'post_type' => 'awsm-project',
        'post_status' => array( 'publish', 'private'),
        'posts_per_page' => 3,
        'orderby' => 'menu_order',
        'order'   => 'DESC',
        // 'tax_query' => array(
        //     array(
        //         'taxonomy' => 'people',
        //         'field'    => 'slug',
        //         'terms'    => 'bob',
        //     ),
        // ),
    );

    // the query
    $projects_query = new WP_Query( $args ); ?>
    
    <?php if ( $projects_query->have_posts() ) : ?>
    
        <!-- pagination here -->
    
        <!-- the loop -->
        <?php while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
            <div class="awsm-project">
                <h2 class="awsm-project__title"><?php the_title(); ?></h2>
                <?php the_post_thumbnail('full'); ?>
            </div>
        <?php endwhile; ?>
        <!-- end of the loop -->
    
        <!-- pagination here -->
    
        <?php wp_reset_postdata(); ?>
    
    <?php else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

</div>
