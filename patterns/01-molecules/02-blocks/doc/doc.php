<?php
	$loop = new WP_Query( array(
		'post_type' => 'awsm-doc',
		'posts_per_page' => -1
	));
?>

<ul>

	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

		<li><h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4></li>

	<?php endwhile; wp_reset_query(); ?>

</ul>
