<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awesomatic
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php 

		if (!is_front_page()) {

			get_template_part( 'patterns/02-organisms/01-article/entry', 'header' );
			
		}

		get_template_part( 'patterns/02-organisms/01-article/entry', 'content' ); 

	?>

</article><!-- #post-<?php the_ID(); ?> -->
